Par projektu
=================

>Datu rīkā apkopota statistika par vidējās izglītības centralizētajiem eksāmeniem Latvijā. 

## Dati

Rīka izveides gaitā tika  apstrādāti dati par 2020. - 2021. gadu, izmantojot [VISC publicētos eksāmenu rezultātus]( https://www.visc.gov.lv/lv/valsts-parbaudes-darbi-statistika/).

Datu rīka izveidē netika nošķirtas dažādas vidusskolu grupas. Dati apkopoti, atsevišķi neizdalot skolas pēc to ģeogrāfiskā novietojuma vai tipa. 

## Autors

Datu rīka autors un īpašnieks ir [SIA Turn Digital](https://www.turn.lv/). Jautājumu, ierosinājumu vai neskaidrību gadījumā lūdzam sazināties ar [Pēteri Jurčenko](mailto:peteris.jurcenko@turn.lv).

Tehnisku jautājumu gadījumā sazināties ar [Zani Rekšņu](mailto:zane@turn.lv)


## Medijiem

Datu rīkā CE rezultāti Latvijā atrodamā informācija ir izmantojama ar atsauci uz avotu.

---

## Izmaiņu vēsture


### Versija 1.0.0.

*2022. gada 6. jūnijs*

- Datu rīks publicēts. [Apskatīt kodu](https://gitlab.com/skolas-platforma/exam-data/skolenu-centralizetie-eksameni/)



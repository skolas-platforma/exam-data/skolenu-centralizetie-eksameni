# install.packages("shiny")
# install.packages("tidyverse")
# install.packages("ggplot2")
# install.packages("shinythemes")

library(shiny)
library(tidyverse)
library(shinythemes)
library(DT)


shiny::navbarPage(
  
  theme = shinytheme ("flatly"),
  header = tags$head(tags$style(HTML(
    ".navbar-default {background-color: #0B0F3F;}
    .navbar-default .navbar-nav>.active>a,
    .navbar-default .navbar-nav>.active>a:hover,
    .navbar-default .navbar-nav>.active>a:focus {background-color: #5f4ae0;}
    
    form.well{background-color: f9f5f0;}
    form.well .schooliowp_logo {    width:auto;    height:auto;    max-width:100%;    max-height:90vh;}
    .nav-tabs>li>a {color: #686868}
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {color: #0B0F3F}
    .exams-header div {font-weight: bold; margin-top: 3rem}
    "
  ))),
  
  "CE rezultāti Latvijas skolās 2020/2021",
  tabPanel(
    "Rezultāti",
    fluidPage(
     sidebarLayout(
       sidebarPanel(
         width = 3,
          selectizeInput("select_skola_results",
                         label = "Izvēlieties skolas salīdzināšanai (maksimums 3)",
                         choices = NULL,
                         multiple = TRUE,
                         options = list(maxItems = 3, placeholder = "Ievadiet skolas nosaukumu",
                                        onInitialize = I('function() { this.setValue(""); }'))),
 
         selectizeInput("select_year",
                        label = "Izvēlieties gadu, kura datus attēlot",
                        choices = NULL,
                        multiple = FALSE,
                        selected = NULL,
                        options = list(maxItems = 1, placeholder = "Izvēlēties gadu",
                                       onInitialize = I('function() { this.setValue(""); }'))),
         

       HTML(
         '       <a href="https://www.schooliowp.com/"><img src="./logo_white_bg.svg" alt="Image here" target = "_blank"/></a>
       '
       ),
       ),
     
     
  
          mainPanel(htmlOutput("pazinojums"),
            textOutput("tabulas_virsraksts"),
            tableOutput("tabula_ar_eksamenu_datiem_results"),
            
            
          tabsetPanel(id = "manitabi",
            # type = "tabs",
                      tabPanel("Kopsavilkums",  plotOutput(outputId = "boxplot_skola")),
                      tabPanel("Obligātie eksāmeni", plotOutput(outputId = "obligatie_eksameni")),
                      tabPanel("Svešvalodu eksāmeni",  plotOutput(outputId = "svesvalodu_eksameni")),
                      tabPanel("Izvēles eksāmeni", plotOutput(outputId = "izveles_eksameni")),
    )
    )
    )
    )
    ),

  
  # SECOND TAB ####
  
tabPanel(
  "Rezultāti pa gadiem",
  fluidPage(
    sidebarPanel(
      width = 3,
      selectizeInput("select_skola_years",
                     label = "Izvēlieties skolas salīdzināšanai (maksimums 3)",
                     choices = NULL,
                     multiple = TRUE,
                     options = list(maxItems = 3, placeholder = "Ievadiet skolas nosaukumu",
                                    onInitialize = I('function() { this.setValue(""); }'))),
    ),
    
    mainPanel(
      
    textOutput("table_name_years"),
    
    tabsetPanel(id = "year_tabs",
                tabPanel(title = "2021/2022"),
                tabPanel(title = "2020/2021"),
                tabPanel(title = "2019/2020"),
                tabPanel(title = "2018/2019"),
                tabPanel(title = "2017/2018"),
                tabPanel(title = "2016/2017"),
                tabPanel(title = "2015/2016"),
                tabPanel(title = "2014/2015")
                
                
                
    ),
    
    
    
    
    tableOutput(outputId = "tabula_ar_eksamenu_datiem_years"),
    
    fluidRow(textOutput("obligatory_exams"), align = "center", class = "exams-header"),
    
    plotOutput(outputId = "obligatory_exams_plot"),
    
    fluidRow(textOutput("foreign_language_exams"), align = "center", class = "exams-header"),
    
    plotOutput(outputId = "foreign_language_exams_plot"),
    
    fluidRow(textOutput("non_obligatory_exams"), align = "center", class = "exams-header"),
    
    plotOutput(outputId = "non_obligatory_exams_plot")
    
    
    
    )
  )
),


tabPanel(
  "Par projektu",
  fluidPage(
    div(includeMarkdown("par_projektu.md"), style= 'max-width:700px;margin:0 auto 70px auto')
  )
  ),


  collapsible = TRUE

)

   
library(shiny)
library(tidyverse)
library(ggplot2)
# library(ggdark)


dalu_seciba <-
  c("Punkti kopā",
    "Pirmā daļa",
    "Otrā daļa",
    "Trešā daļa",
    "Ceturtā daļa",
    "Piektā daļa")

eksamenu_seciba <- c(
  "Latviešu valoda",
  "Matemātika",
  "Angļu valoda",
  "Krievu valoda",
  "Bioloģija",
  "Fizika",
  "Ķīmija",
  "Vēsture",
  "Vācu valoda",
  "Franču valoda"
)



# GETTING DATA AND MUTATING IT ####

exam_data_all_clean <- read_csv("data/exam_all_years.csv") %>%
  mutate(dala = fct_relevel(dala, dalu_seciba)) %>%
  mutate(eksamens = fct_relevel(eksamens, eksamenu_seciba)) %>%
  mutate(gads = factor(gads))

function(input, output, session) {
  
  
  # OBSERVE EVENTS ####
  
  observeEvent(input$select_skola_results,
               
                 {
                   updateSelectInput(session,
                                     'select_skola_years',
                                     selected = input$select_skola_results)
               })
  
  
  observe({
    
    
    if (!isTruthy(input$select_skola_results)) {
      output$pazinojums <- renderUI(includeHTML("pazinojums.html"))
      hideTab(inputId = "manitabi", target = "Kopsavilkums")
      hideTab(inputId = "manitabi", target = "Obligātie eksāmeni")
      hideTab(inputId = "manitabi", target = "Svešvalodu eksāmeni")
      hideTab(inputId = "manitabi", target = "Izvēles eksāmeni")
    } else{
      output$pazinojums <- NULL
      showTab(inputId = "manitabi", target = "Kopsavilkums")
      showTab(inputId = "manitabi", target = "Obligātie eksāmeni")
      showTab(inputId = "manitabi", target = "Svešvalodu eksāmeni")
      showTab(inputId = "manitabi", target = "Izvēles eksāmeni")
      
    }
    return()
  })
  

  # UPDATE SELECT INPUTS EVENTS  ####
  
  updateSelectInput(session,
                    "select_skola_results",
                    choices = unique(exam_data_all_clean$skola)
                    )
  
  updateSelectInput(session,
                    "select_skola_years",
                    choices = unique(exam_data_all_clean$skola)
  )
  
  updateSelectInput(session,
                    "select_year",
                    choices = unique(exam_data_all_clean$gads),
                    selected = unique(exam_data_all_clean$gads[1])
  )
  

  
    
# TAB 1 FUNCTIONS ####    
    
  output$filtra_banneris <-
    renderUI(includeHTML("filtra_banneris.html"))
  
  output$tabulas_virsraksts <- renderText({
    req(input$select_skola_results)
    "Skolēnu skaits, kuri kārtoja attiecīgo eksāmenu"
  })
  
  
  output$tabula_ar_eksamenu_datiem_results <- renderTable({
    req(input$select_year)
    req(input$select_skola_results)
   tabula_ar_eksamenu_datiem <- exam_data_all_clean %>%
     filter(gads == input$select_year) %>%
      select(skola, eksamens, nr.) %>%
      filter(skola %in% input$select_skola_results)%>%
      unique() %>%
      group_by(skola, eksamens) %>%
      summarise(eksamens = eksamens, skaits = n()) %>%
      unique() %>%
      pivot_wider(names_from = eksamens, values_from = skaits)
  }, na = "0",
  striped = TRUE,
  spacing = "s")
  
  
  output$boxplot_skola <- renderPlot({
    req(input$select_year)
    req(input$select_skola_results)
    exam_data_all_clean %>%
      filter(gads == input$select_year) %>%
      filter(skola %in% input$select_skola_results)%>%
      complete(skola, eksamens) %>% # Uztaisa NA rindas skolām, kas nav kārtoujšas eksāmenu https://github.com/tidyverse/ggplot2/issues/3345
      mutate_at(vars(procenti), ~ replace_na(., 200)) %>% # NA aizstāj ar vērtību, kas būs ārpus skalas
      ggplot(aes(x = procenti, y = eksamens, fill = skola)) +
      geom_boxplot(position = position_dodge(0.75), width = 0.5)+
      coord_cartesian(xlim = c(1, 100))+ # Sakal no 1 līdz 100, lai nerāda vērtības ārpus skalas
      scale_x_continuous(position = "top") +
      scale_y_discrete(limits = rev, name = NULL) +
      scale_fill_discrete(limits = rev) +
      #     dark_mode(theme_minimal()) +
      theme_bw()+
      theme(
        axis.title.x = element_text(size = 16),
        axis.title.y = element_blank(),
        legend.position = "top",
        axis.text.y = element_text(size = 14),
        axis.text.x = element_text(size = 14)
        # legend.text = element_text(size = 12)
      )+
      scale_fill_manual(values = c("#ff7a86", "#f5b30a", "#9d91ec"))
  #
  }, height = 700)
  
  output$obligatie_eksameni <- renderPlot({
    req(input$select_year)
    req(input$select_skola_results)
    exam_data_all_clean %>%
      filter(gads == input$select_year) %>%
      filter(skola %in% input$select_skola_results) %>%
      filter(eksamens %in% c("Matemātika", "Latviešu valoda")) %>%
      filter(!is.na(punkti)) %>%
      ggplot(aes(x = punkti, y = dala, fill = skola)) +
      geom_boxplot() +
      scale_y_discrete(limits = rev) +
      facet_wrap( ~ eksamens) +
      theme_bw()+
      theme(
        axis.title.x = element_text(size = 16),
        axis.title.y = element_blank(),
        legend.position = "top",
        axis.text.y = element_text(size = 14),
        axis.text.x = element_text(size = 14)
        # legend.text = element_text(size = 12)
      )+
      scale_fill_manual(values = c("#ff7a86", "#f5b30a", "#9d91ec"))
    # 
  },
  # height = 700
  )
  
  output$svesvalodu_eksameni <- renderPlot({
    req(input$select_year)
    req(input$select_skola_results)
    exam_data_all_clean %>%
      filter(gads == input$select_year) %>%
      filter(skola %in% input$select_skola_results) %>%
      filter(eksamens %in% c(
        "Angļu valoda",
        "Vācu valoda",
        "Krievu valoda",
        "Franču valoda"
      )) %>%
      filter(!is.na(punkti)) %>%
      ggplot(aes(x = punkti, y = dala, fill = skola)) +
      geom_boxplot() +
      scale_y_discrete(limits = rev) +
      facet_wrap( ~ eksamens) +
      theme_bw()+
      theme(
        axis.title.x = element_text(size = 16),
        axis.title.y = element_blank(),
        legend.position = "top",
        axis.text.y = element_text(size = 14),
        axis.text.x = element_text(size = 14)
        # legend.text = element_text(size = 12)
      )+
      scale_fill_manual(values = c("#ff7a86", "#f5b30a", "#9d91ec"))
    # 
  },
  # height = 700
  )
  
  output$izveles_eksameni <- renderPlot({
    req(input$select_year)
    req(input$select_skola_results)
    exam_data_all_clean %>%
      filter(gads == input$select_year) %>%
      filter(skola %in% input$select_skola_results) %>%
      filter(eksamens %in% c("Bioloģija", "Fizika", "Ķīmija", "Vēsture")) %>%
      filter(!is.na(punkti)) %>%
      ggplot(aes(x = punkti, y = dala, fill = skola)) +
      geom_boxplot() +
      scale_y_discrete(limits = rev) +
      facet_wrap( ~ eksamens) +
      theme_bw()+
      theme(
        plot.title = element_text(size = 16, face = "bold"),
        axis.title.x = element_text(size = 16),
        axis.title.y = element_blank(),
        legend.position = "top",
        axis.text.y = element_text(size = 14),
        axis.text.x = element_text(size = 14)
        # legend.text = element_text(size = 12)
      )+
      scale_fill_manual(values = c("#ff7a86", "#f5b30a", "#9d91ec"))
    # 
  },
  # height = 700
  )
  
  
  # TAB 2 PLOT FUNCTIONS ####
  
  output$table_name_years <- renderText({
    req(input$select_skola_years)
    "Skolēnu skaits, kuri kārtoja attiecīgo eksāmenu"
  })
  
  
  output$obligatory_exams <- renderText({
    req(input$select_skola_years)
    "Obligātie eksāmeni"
  })
  
  output$foreign_language_exams <- renderText({
    req(input$select_skola_years)
    "Svešvalodu eksāmeni"
  }) 
  
  output$non_obligatory_exams <- renderText({
    req(input$select_skola_years)
    "Izvēles eksāmeni"
    
  })
  
  
  output$tabula_ar_eksamenu_datiem_years <- renderTable({
    req(input$select_skola_years)
    req(input$year_tabs)
    tabula_ar_eksamenu_datiem <- exam_data_all_clean %>%
      filter(gads == input$year_tabs) %>%
      select(skola, eksamens, nr.) %>%
      filter(skola %in% input$select_skola_years)%>%
      unique() %>%
      group_by(skola, eksamens) %>%
      summarise(eksamens = eksamens, skaits = n()) %>%
      unique() %>%
      pivot_wider(names_from = eksamens, values_from = skaits)
  }, na = "0",
  striped = TRUE,
  spacing = "s")
  
  
  output$obligatory_exams_plot <- renderPlot({
    req(input$select_skola_years)
    exam_data_all_clean %>%
      filter(skola %in% input$select_skola_years) %>%
      filter(eksamens %in% c("Matemātika", "Latviešu valoda")) %>%
      filter(!is.na(punkti)) %>%
      mutate_at(vars(procenti), ~ replace_na(., 200)) %>% # NA aizstāj ar vērtību, kas būs ārpus skalas
      ggplot(aes(x = procenti, y = gads, fill = skola)) +
      geom_boxplot(position = position_dodge(0.75), width = 0.5)+
      coord_cartesian(xlim = c(1, 100))+ # Sakal no 1 līdz 100, lai nerāda vērtības ārpus skalas
      scale_x_continuous(position = "top") +
      scale_fill_discrete(limits = rev) +
      facet_wrap( ~ eksamens, ncol = 2) +
      #     dark_mode(theme_minimal()) +
      theme_bw()+
      theme(
        axis.title.x = element_text(size = 16),
        axis.title.y = element_blank(),
        legend.position = "top",
        axis.text.y = element_text(size = 14),
        axis.text.x = element_text(size = 14)
        # legend.text = element_text(size = 12)
      )+
      scale_fill_manual(values = c("#ff7a86", "#f5b30a", "#9d91ec"))
    #
  }, height = 400)
  
  output$foreign_language_exams_plot <- renderPlot({
    req(input$select_skola_years)
    exam_data_all_clean %>%
      filter(skola %in% input$select_skola_years) %>%
      filter(eksamens %in% c(
        "Angļu valoda",
        "Vācu valoda",
        "Krievu valoda",
        "Franču valoda"
      )) %>%
      filter(!is.na(punkti)) %>%
      mutate_at(vars(procenti), ~ replace_na(., 200)) %>% # NA aizstāj ar vērtību, kas būs ārpus skalas
      ggplot(aes(x = procenti, y = gads, fill = skola)) +
      geom_boxplot(position = position_dodge(0.75), width = 0.5)+
      coord_cartesian(xlim = c(1, 100))+ # Sakal no 1 līdz 100, lai nerāda vērtības ārpus skalas
      scale_x_continuous(position = "top") +
      scale_fill_discrete(limits = rev) +
      facet_wrap( ~ eksamens, ncol = 4) +
      #     dark_mode(theme_minimal()) +
      theme_bw()+
      theme(
        axis.title.x = element_text(size = 16),
        axis.title.y = element_blank(),
        legend.position = "top",
        axis.text.y = element_text(size = 14),
        axis.text.x = element_text(size = 14)
        # legend.text = element_text(size = 12)
      )+
      scale_fill_manual(values = c("#ff7a86", "#f5b30a", "#9d91ec"))
    #
  }, height = 400)
  
  output$non_obligatory_exams_plot <- renderPlot({
    req(input$select_skola_years)
    exam_data_all_clean %>%
      filter(skola %in% input$select_skola_years) %>%
      filter(eksamens %in% c("Bioloģija", "Fizika", "Ķīmija", "Vēsture")) %>%
      filter(!is.na(punkti)) %>%
      mutate_at(vars(procenti), ~ replace_na(., 200)) %>% # NA aizstāj ar vērtību, kas būs ārpus skalas
      ggplot(aes(x = procenti, y = gads, fill = skola)) +
      geom_boxplot(position = position_dodge(0.75), width = 0.5)+
      coord_cartesian(xlim = c(1, 100))+ # Sakal no 1 līdz 100, lai nerāda vērtības ārpus skalas
      scale_x_continuous(position = "top") +
      scale_fill_discrete(limits = rev) +
      facet_wrap( ~ eksamens, ncol = 4) +
      #     dark_mode(theme_minimal()) +
      theme_bw()+
      theme(
        axis.title.x = element_text(size = 16),
        axis.title.y = element_blank(),
        legend.position = "top",
        axis.text.y = element_text(size = 14),
        axis.text.x = element_text(size = 14)
        # legend.text = element_text(size = 12)
      )+
      scale_fill_manual(values = c("#ff7a86", "#f5b30a", "#9d91ec"))
    #
  }, height = 400)
  

  # DT tabula sākums
  
  output$Tabula_ar_visu_skolu_datiem = DT::renderDataTable({
    Tabula_ar_visu_skolu_datiem <- exam_data_all_clean %>%
      select(skola, eksamens, nr.) %>%
      filter(eksamens == "Matemātika") %>%
      unique() %>%
      group_by(skola) %>%
      summarise("skola" = skola, "skolēnu skaits" = n()) %>%
      unique()%>%
      datatable(rownames = FALSE,
                filter = "top",
                extensions = "Responsive",
                options = list(
                  pageLength = 10,
                  language = list(url = '//cdn.datatables.net/plug-ins/1.10.20/i18n/Latvian.json') 
                )
      )
    
    
  })
  
  # DT tabula beigas
 
}


